
#ifndef LIBLEDS_H
#define LIBLEDS_H

#include <stdint.h>

extern void     init_reg_leds(void);
extern uint32_t get_reg_leds(void);
extern void     set_reg_leds(uint32_t val);

#endif /* LIBLEDS_H */
